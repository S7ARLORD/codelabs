<!DOCTYPE html>
<html>
<head>
	<title>About</title>
	<style type="text/css">
	.parallax-text {
  position: absolute;
  color: white;
  font-size: 2em;
}	
	</style>
	<?php require 'header.php' ?>

</head>
<body>

<?php  require'nav.php' ?>
 <!-- <div class="row">
        <div class="col s12 m12">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text h2">
              <span class="card-title h2">About us</span>
              <p class="!important">To help students of every branch be better coder, better thinker and the best innovator for a Bigger & Brighter Future</p>
              <h3 class="right-align">To help students of every branch be better coder,<br> better thinker and the best innovator for a Bigger & Brighter Future</h3>
            </div>
          
          </div>
        </div>
      </div> -->
       <div class="parallax-container">
		      <div class="parallax">
		      	<img src="Images/seminar.jpg">
		      	<div class="parallax-text">To help students of every branch be better coder,better thinker and the best innovator for a Bigger & Brighter Future</div>
		      	
		      </div>
        </div>

  
  
<div class="container">
	<div class="row">
		<div class="col s12 m5 offset-m3" style="float: center; font-size:56px; font-weight: 100;">
			Meet Our Team
		</div>
		
	</div>
	      <div class="row">
      	<div class="col s12 m4 offset-m4">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/kalpana 002.jpg" class="responsive-img circle center">
          <span class="card-title">Kalpana Deorukhkar</span>
         <span>Faculty Incharge</span>
         
        </div>
         <div class="card-action">
              <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
      </div>

<div class="row">
	<div class="col s12 m4">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/samarth.jpg" class="responsive-img circle center">
          <span class="card-title">Samarth Gupta</span>
         <span>Chairperson</span>
         
        </div>
         <div class="card-action">
                <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>

      	<div class="col s12 m4 ">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/mel.jpg" class="responsive-img circle center">
          <span class="card-title">Melita Seldhanha</span>
         <span>Vice Chairperson</span>
         
        </div>
         <div class="card-action">
               <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>

      	<div class="col s12 m4 ">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/akash.jpg" class="responsive-img circle center">
          <span class="card-title">Akash Palghadmal</span>
         <span>Vice Chairperson</span>
         
        </div>
         <div class="card-action">
               <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
</div>


<div class="row">
	<div class="col s12 m4 ">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/bhanu.jpg" class="responsive-img circle center">
          <span class="card-title">Bhanugoban Nadar</span>
         <span>Problem Setter/Tester</span>
         
        </div>
         <div class="card-action">
               <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
      	<div class="col s12 m4 ">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/mel.jpg" class="responsive-img circle center">
          <span class="card-title">Rathil Patel</span>
         <span>Webmaster</span>
         
        </div>
         <div class="card-action">
               <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
      	<div class="col s12 m4">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/manu.png" class="responsive-img circle center">
          <span class="card-title">Manupendra Tiwari</span>
         <span>Design Head</span>
         
        </div>
         <div class="card-action">
               <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
</div>

<div class="row">
	<div class="col s12 m4 offset-m2">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/glenice.jpg" class="responsive-img circle center">
          <span class="card-title">Glenice D'sa</span>
         <span>Marketing Head</span>
         
        </div>
         <div class="card-action">
               <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div><div class="col s12 m4 offset-m1">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/fascel.png" class="responsive-img circle center">
          <span class="card-title">Fascel Fernandes</span>
         <span>PR Head</span>
         
        </div>
         <div class="card-action">
               <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
</div>


<div class="row">
	<div class="col s12 m4 ">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/summit.jpg" class="responsive-img circle center">
          <span class="card-title">Summit Gupta</span>
         <span>Problem Setter/Tester</span>
         
        </div>
         <div class="card-action">
               <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
      	<div class="col s12 m4 ">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/ishpreet.jpg" class="responsive-img circle center">
          <span class="card-title">Ishpreet Dham</span>
         <span>SE Representative</span>
         
        </div>
         <div class="card-action">
            <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
      	<div class="col s12 m4 ">
      <div class="card hoverable" >
        <div class="card-content">
         <img src="Images/team/clayton.jpg" class="responsive-img circle center">
          <span class="card-title">Clayton Pereira</span>
         <span>SE Representative</span>
         
        </div>
         <div class="card-action">
              <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
              <a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>

      </div>
      	</div>
</div>
 </div>
</div>
<?php require'footer.php' ?>
</body>
</html>