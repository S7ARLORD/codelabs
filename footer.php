 <footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
              </div>
            </div>
          </div>
           <div class="row">
    <div class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">email</i>
          <input id="email" type="email"  class="validate">
            <label for="email" data-error="wrong" data-success="right">Email</label>
        </div>
      </div>
    </div>
  </div>
          <div class="footer-copyright">
            <div class="container">
            © 2017-18 Codelabscrce,All Rights Reserved
            <a class="grey-text text-lighten-4 right" href="#!">Designed and Developed by Rathil Patel</a>
            </div>
          </div>
        </footer>
            